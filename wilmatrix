#!/usr/bin/perl -w

# Config file is $HOME/.config/wilmatrix/wilmatrix.conf and it contains lines such as:
#
# domain=foo.inschool.fi
# user=my.wilma@user.id
# pass=my_wilma_password
# kid0=1234
# kid1=5678
# loop=1000
# matrix_url0=https://homeserver/_matrix/client/r0/rooms/%21ROOM_ID_HERE:matrix.domain/send/m.room.message?access_token=ACCESS_TOKEN_HERE
# matrix_url1=https://homeserver/_matrix/client/r0/rooms/%21OTHER_ROOM_ID_HERE:matrix.domain/send/m.room.message?access_token=ACCESS_TOKEN_HERE
# matrix_url_messages0=https://homeserver/_matrix/client/r0/rooms/%21ROOM_ID_HERE:matrix.domain/send/m.room.message?access_token=ACCESS_TOKEN_HERE
# matrix_url_messages1=https://homeserver/_matrix/client/r0/rooms/%21OTHER_ROOM_ID_HERE:matrix.domain/send/m.room.message?access_token=ACCESS_TOKEN_HERE
# matrix_url_news0=https://homeserver/_matrix/client/r0/rooms/%21ROOM_ID_HERE:matrix.domain/send/m.room.message?access_token=ACCESS_TOKEN_HERE
# matrix_url_news1=https://homeserver/_matrix/client/r0/rooms/%21OTHER_ROOM_ID_HERE:matrix.domain/send/m.room.message?access_token=ACCESS_TOKEN_HERE
# matrix_url_attendance0=https://...
# newmessages=wilma | wilmatrix

# matrix_urlN is default for matrix_url_messagesN and matrix_url_newsN if 
# you want both messages and news to same room.

# kid0 and kid1 are numbers from wilma urls, for example https://foo.inschool.fi/!1234567/messages

# newmessages=wilma 		Use wilma's message list json to detect new messages
# newmessages=wilmatrix 	Keep track of read messages in wilmatrix

# useragent=MyBrowser

# Get an access token for wilmatrix with curl:
# curl -XPOST -d '{"type":"m.login.password", "user":"USERNAME","password":"PASSWORD"}' https://homeserver/_matrix/client/r0/login
#
# Install dependencies:
# sudo apt install libfile-homedir-perl libhtml-formattext-withlinks-perl libhtml-parser-perl libhttp-cookies-perl libjson-perl libwww-perl
#
# Optional dependencies:
# For reading password from terminal without echo: sudo apt install libterm-readkey-perl

use strict;
use utf8;
use LWP::UserAgent;
use HTML::Parser;
use HTML::FormatText::WithLinks;
use HTTP::Cookies;
use JSON;
use Fcntl qw(O_RDWR O_CREAT);
use NDBM_File;
use Module::Load::Conditional 'can_load';

my $useragent='FZf8ZmY';
my $ua;
my ($domain,$w,$user,$pass);
my (@matrix_url, @matrix_url_messages, @matrix_url_news, @matrix_url_attendance);
my %matrix_url=('',\@matrix_url,'messages',\@matrix_url_messages,'news',\@matrix_url_news,'attendance',\@matrix_url_attendance);
my @kid_id;
my @messagelist;
my @handler_state;
my %handler_skip=(qw(br 1 link 1 meta 1 input 1 img 1 col 1));
my @news_title;
my $news_title;
my %news_link;
my @attendance;
my $attendance_td_counter=-1;
my $attendance_weekday;
my $attendance_date;
my $attendance_tag;
my %attendance_expl;

my $sessionid;
my $logout_value;
my $cookie_jar;
my $debug='';
my $htmlwarnings='';
my $stdout='';
my $confdir;
my $loop=0;
my $newmessages='wilma';
my $login_done='';

my $use_list={ 'File::HomeDir' => undef };
if (can_load(modules => $use_list, autoload => 1)) {
	$confdir=File::HomeDir->my_home . '/.config/wilmatrix/';
} else {
	$confdir=$ENV{'HOME'}.'/.config/wilmatrix/';
}

sub debug_headers {
	$ua->add_handler(request_send => sub {
				 my ($r, $ua, $h)=@_;
				 my @n=($r->headers->header_field_names);
				 for my $n (@n) {
					 warn "request header: $n: ",$r->headers->header($n),"\n";
				 }
			 });
	$ua->add_handler(response_done => sub {
				 my ($r, $ua, $h)=@_;
				 my @n=($r->headers->header_field_names);
				 for my $n (@n) {
					 warn "response header: $n: ",$r->headers->header($n),"\n";
				 }
			 });
}

sub read_config {
	my $conf=$confdir.'wilmatrix.conf';
	open(C,'<',$conf) or die "Can't open $conf: $!\n";
	while(defined(my $l=<C>)) {
		if($l=~/^\s*w\s*=\s*(.*?)\s*$/) {
			$w=$1;
		}elsif($l=~/^\s*user\s*=\s*(.*?)\s*$/) {
			$user=$1;
		}elsif($l=~/^\s*pass\s*=\s*(.*?)\s*$/) {
			$pass=$1;
		}elsif($l=~/^\s*domain\s*=\s*(.*?)\s*$/) {
			$domain=$1;
		}elsif($l=~/^\s*matrix_url_?([a-z]*)_?(\d+)\s*=\s*(.*?)\s*$/) {
			my $aref=$matrix_url{$1};
			$aref->[$2]=$3;
			warn "Found matrix url for message type \"$1\" for kid index $2.\n" if $debug;
		}elsif($l=~/^\s*kid(\d+)\s*=\s*(\d+)\s*$/) {
			$kid_id[$1]=$2;
		}elsif($l=~/^\s*loop\s*=\s*(\d+)\s*$/) {
			$loop=$1;
		}elsif($l=~/^\s*newmessages\s*=\s*(.+)\s*$/) {
			$newmessages=$1;
		}elsif($l=~/^\s*useragent\s*=\s*(.+)\s*$/) {
			$useragent=$1;
		}elsif($l=~/^\s*$/ or $l=~/^\s*#/) {
			#skip
		}else{
			warn "$0: Warning: unknown line in $conf: $l";
		}
	}
	defined($domain) or die "No domain in $conf";
	defined($user) or die "No user in $conf";
	if(!defined($w)) {
		$w='https://'.$domain.'/';
	}elsif(substr($w,-1,1) ne '/') {
		$w.='/';
	}
	if(!defined($pass)) {
		warn "No Wilma password in config file\n";
		$use_list={ 'Term::ReadKey' => undef };
		if(can_load(modules => $use_list, autoload => 1)) {
			print STDERR "Wilma password:";
			ReadMode('noecho');
			$pass=ReadLine(0);
			ReadMode('restore');
		} else {
			warn "Reading wilma password from stdin\n";
			$pass=<STDIN>;
		}
		chomp $pass;
		print STDERR "\n";
		if(!defined($pass) or '' eq $pass ) {
			die "No password";
		}
		exit 0;
	}
	my $cookie_file=$confdir.'cookies.txt';
	$cookie_jar=HTTP::Cookies->new(file => $cookie_file, autosave => 1);
	$ua=LWP::UserAgent->new(agent => $useragent, requests_redirectable =>['GET', 'HEAD','POST']);
	$ua->cookie_jar($cookie_jar);
}

sub get_sessionid {
	my $url=$w.'login';
	my $r=$ua->get($url);
	$r->is_success or die "Didn't get login page: ",$r->code,$r->message,$r->content;
	&parse_sessionid($r->decoded_content);
	$cookie_jar->save;
}

sub sessionid_handler {
	return unless shift eq 'input';
	my $href=shift;
	my $type=$href->{'type'};
	my $name=$href->{'name'};
	my $value=$href->{'value'};
	(defined($type) and defined($name)) or return;
	if($type eq 'hidden' and $name eq 'SESSIONID') {
		$sessionid=$value;
		#$cookie_jar->set_cookie(0, 'Wilma2LoginID',$sessionid,'/',$domain,'443',1,1,10000,0,{});
	}
}

sub parse_sessionid {
	my $html=shift;
	my $p=HTML::Parser->new(api_version =>3, start_h => [\&sessionid_handler, "tagname,attr"] );
	defined($p) or die $!;
	defined($html) or die $!;
	$p->parse($html);
	return defined($sessionid);
}

sub parse_logout_handler {
	return unless shift eq 'input';
	my $href=shift;
	my $type=$href->{'type'};
	my $name=$href->{'name'};
	my $value=$href->{'value'};
	(defined($type) and defined($name) and defined($value)) or return;
	if($type eq 'hidden' and $name eq 'formkey') {
		$logout_value=$value;
		warn "logout=$logout_value\n" if $debug;
	}
}

sub parse_logout {
	my $html=shift;
	my $p=HTML::Parser->new(api_version=>3, start_h => [\&parse_logout_handler, "tagname,attr"]);
	defined($p) or die $!;
	defined($html) or die $!;
	$p->parse($html);
	return defined($logout_value);
}


sub news_handler_start {
	my ($p, $tag, $ref, $path)=&handler_start_common;
	if($tag eq 'a' and $path=~/html body (main )?div div div div div div span a/) {
		if($news_title) {
			push @news_title, $news_title;
			$news_link{$news_title}=$ref->{'href'};
			$news_title='';
		}
	}elsif($tag eq 'h2' and $path=~/html body (main )?div div div div div h2/) {
		$p->handler(text => sub {
				    my ($p, $text)=@_;
				    if($text=~/^\s*(.*?)\s*$/s) {
					    $news_title=$1;
				    }
				    $p->handler(text => undef);
			    }, 'self,dtext');
	}elsif($tag eq 'h3' and $path=~/html body (main )?div div div div div div h3/) {
		$p->handler(text => sub {
				    my ($p, $text)=@_;
				    if($text=~/^\s*(.*?)\s*$/s) {
					    $news_title.=' '.$1;
				    }
				    $p->handler(text => undef);
			    }, 'self,dtext');
	}
}

sub attendance_handler_start {
	my ($p, $tag, $ref, $path)=&handler_start_common;
	if ($tag eq 'tr' and $path eq 'html body div div div div div div div div table tbody tr') {
		$attendance_td_counter=0;
		warn "counter reset" if $debug;
	} elsif ($tag eq 'div') {
		my $class=$ref->{'class'};
		if (defined($class) and $class eq 'legend') {
			$p->handler(start => undef);
			$p->handler(start => sub {
					    my ($p, $tag, $ref, $path)=&handler_start_common;
					    my $class=$ref->{'class'};
					    if (defined($class) and $class=~/(at-tp\d+)/) {
						    $attendance_tag=$1;
						    $p->handler(text => sub {
									my ($p,$text)=@_;
									$attendance_expl{$attendance_tag}=$text;
									warn $text if $debug;
									$p->handler(text => undef);
								},'self,text');
					    }
				    }
				    , 'self,tagname,attr');
		}
	}
	if ($attendance_td_counter > -1 and $tag eq 'td') {
		++$attendance_td_counter;
		my $class=$ref->{'class'};
		my $title=$ref->{'title'};
		if ($attendance_td_counter==1) {
			$p->handler(text => sub {
					    my ($p,$text)=@_;
					    $attendance_weekday=$text;
					    $p->handler(text => undef);
				    },'self,dtext');
		} elsif ($attendance_td_counter==2) {
			$p->handler(text => sub {
					    my ($p,$text)=@_;
					    $attendance_date=$text;
					    $p->handler(text => undef);
				    },'self,dtext');
		} elsif ($attendance_td_counter > 2 and defined($class) and defined($title)) {
			my $a={};
			push @attendance, $a;
			$a->{'wd'}=$attendance_weekday;
			$a->{'date'}=$attendance_date;
			$a->{'column'}=$attendance_td_counter;
			if ($class =~ /(at-tp\d+)/) {
				$a->{'class'}=$1;
			}
			$a->{'title'}=$title;
			$p->handler(text => sub {
					    my ($p,$text)=@_;
					    $attendance[-1]->{'text'}=$text;
					    $p->handler(text => undef);
				    },'self,dtext');
		}
	} elsif ($attendance_td_counter > -1 and $tag eq 'sup') {
		$p->handler(text => sub {
				    my ($p,$text)=@_;
				    $attendance[-1]->{'sup'}=$text;
				    $p->handler(text => undef);
				    });
	}
}

sub handler_start_common {
	my ($p, $tag, $ref)=@_;
	if(!$handler_skip{$tag}) {
		push @handler_state, $tag;
	}
	my $path="@handler_state";
	warn "$path\n" if $debug;
	return ($p, $tag, $ref, $path);
}

sub handler_end {
	if($_[0] eq $handler_state[-1]) {
		pop @handler_state;
	}else{
		warn "tag $_[0] ended, but was expecting $handler_state[-1]\n" if $htmlwarnings;
	}
}

sub parse_news {
	my $ref=shift;
	ref $ref eq 'SCALAR' or die;
	my $p=HTML::Parser->new(api_version =>3);
	$p->handler(start => \&news_handler_start, 'self,tagname,attr');
	$p->handler(end => \&handler_end, 'tagname');
	@handler_state=();
	$p->parse($$ref);
	return scalar(@news_title);
}

sub parse_attendance {
	my $ref=shift;
	ref $ref eq 'SCALAR' or die;
	my $p=HTML::Parser->new(api_version => 3);
        $p->handler(start => \&attendance_handler_start, 'self,tagname,attr');
	$p->handler(end => sub {
			    &handler_end;
			    if($_[0] eq 'tr') {
				    $attendance_td_counter=-1;
			    }
		    }, 'tagname');
	@attendance=();
	@handler_state=();
	$p->parse($$ref);
	return scalar(@attendance);
}

sub post_login {
	my $r=$ua->post($w.'login',
			[
			'Login',$user,
			'Password',$pass,
			'submit','Kirjaudu sisään',
			'SESSIONID',$sessionid,
			'returnpath',''
			 ]
			);
	if($r->is_success) {
		warn "login: ",$r->code,' ',$r->message,"\n" if $debug;
		&parse_logout($r->decoded_content);
		return 1;
	}
	if($r->code == 303) {
		warn "303";
		return 1;
	}
	warn $r->code,' ',$r->message;
	$login_done=1;
	return '';
}

sub post_logout {
	if(!defined($logout_value)) {
		warn "Don't know how to logout\n";
		return;
	}
	my $r=$ua->post($w.'logout',
			[
			 'formkey',$logout_value
			]);
	warn "logout: ",$r->code,' ',$r->message if $debug;
}

sub kid_url {
	my $kid_index=shift;
	my $kid_id=$kid_id[$kid_index];
	return $w.'!'.$kid_id.shift;
}

sub get_message_list {
	my $kid_index=$_[0];
	my $url=&kid_url(shift,'/messages/list');
	my $r=$ua->get($url);
	if(!$r->is_success) {
		warn "Didn't get message list from url \"$url\": ",$r->code,' ',$r->message,' ',$r->decoded_content;
		return '';
	}
	$messagelist[$kid_index]=decode_json($r->decoded_content);
	return 1;
}

sub get_news {
	my $url=&kid_url(shift,'/news');
	my $r=$ua->get($url);
	if(!$r->is_success) {
		warn "Didn't get news list from url \"$url\": ",$r->code,' ',$r->message,' ',$r->decoded_content;
		return;
	}
	return $r->decoded_content;
}

sub get_attendance {
	my $url=&kid_url(shift,'/attendance/view?range=0');
	my $r=$ua->get($url);
	if(!$r->is_success) {
		warn "Didn't get attendance view from url \"$url\": ",$r->code,' ',$r->message,' ',$r->decoded_content;
		return;
	}
	return $r->decoded_content;
}

sub new_messages_wilma {
	my @new;
	my $kid_index=shift;
	my $j=$messagelist[$kid_index];
	my $msgs=$j->{'Messages'};
	for my $mo (@{$msgs}) {
		my $status=$mo->{'Status'};
		if(defined($status) and $status) {
			push @new,$mo->{'Id'};
		}
	}
	return @new;
}

sub new_messages_wilmatrix {
	my @new;
	my $kid_index=shift;
	my $href=shift;
	my $j=$messagelist[$kid_index];
	my $msgs=$j->{'Messages'};
	for my $mo (@{$msgs}) {
		my $msgid=$mo->{'Id'};
		if(!defined($href->{$msgid})) {
			push @new,$msgid;
		}
	}
	return @new;
}

sub mark_message_unread {
	my $kid_index=shift;
	my $message_id=shift;
	my $file=$confdir."messages_forwarded_$kid_index";
	tie(my %messages_forwarded, 'NDBM_File', $file, O_RDWR, 0600) or return;
	delete $messages_forwarded{$message_id};
}

sub mark_all_messages_as_read {
	my $kid_index=shift;
	my $file=$confdir."messages_forwarded_$kid_index";
	tie(my %messages_forwarded, 'NDBM_File', $file, O_RDWR|O_CREAT, 0600);
	my $j=$messagelist[$kid_index];
	my $msgs=$j->{'Messages'};
	for my $mo (@{$msgs}) {
		my $msgid=$mo->{'Id'};
		if(!defined($messages_forwarded{$msgid})) {
			warn "Marking $msgid as read\n";
			$messages_forwarded{$msgid}=time;
		}
	}
}

sub list_read_messages {
	my $kid_index=-1;
	while (1) {
		++$kid_index;
		my $file=$confdir."messages_forwarded_$kid_index";
		tie(my %messages_forwarded, 'NDBM_File', $file, O_RDWR, 0600) or last;
		print "Kid $kid_index\n";
		while (my ($id,$time)=each %messages_forwarded) {
			print "$id $time\n";
		}
		print "\n";
	}
}

sub get_message {
	#https://xxx.wilma.yyy.fi/!1234/messages/56789?printable
	my ($kid_index,$message_id)=@_;
	my $r=$ua->get("${w}!${kid_id[$kid_index]}/messages/${message_id}?printable");
	if(!$r->code == 200) {
		warn "get_message: ",$r->code," ",$r->message;
		return '';
	}
	return $r->decoded_content;
}

sub parse_message {
	my $html=shift;
	if($html=~/.*<body>(.*)<\/body>/s) {
		my $body=$1;
		my $f = HTML::FormatText::WithLinks->new();
		my $text = $f->parse($body);
		return ($text,$body);
	}
}

sub send_html {
	my ($kid_index,$text,$html,$message_type)=@_;
	if($stdout) {
		print "--stdout given on command line. NOT posting the following message to matrix:\n";
		print "$text\n";
		print "$html\n";
		return 1;
	}

	my $j={};
	$j->{'msgtype'}='m.text';
	$j->{'format'}='org.matrix.custom.html';
	$j->{'body'}=$text;
	$j->{'formatted_body'}=$html;
	my $data=encode_json($j);
	my $matrix_url=&get_matrix_url($kid_index,$message_type);
	my $r=$ua->post($matrix_url, Content => $data);
}

sub send_text {
	my ($kid_index,$text,$message_type)=@_;
	if($stdout) {
		print "--stdout given on command line. NOT posting the following message to matrix:\n";
		print "$text\n";
		return 1;
	}
	my $j={};
	$j->{'msgtype'}='m.text';
	$j->{'body'}=$text;
	my $data=encode_json($j);
	my $matrix_url=&get_matrix_url($kid_index,$message_type);
	my $r=$ua->post($matrix_url, Content => $data);
}

sub get_matrix_url {
	my ($kid_index,$message_type)=@_;
	my $aref=$matrix_url{$message_type};
	my $url=$aref->[$kid_index];
	defined($url) and return $url;
	$url=$matrix_url[$kid_index];
	defined($url) and return $url;
	die "Didn't find matrix url for kid index $kid_index message type $message_type\n";
}

sub forward_messages {
	my $kid_index=shift;
	&get_message_list($kid_index);
	my @new;
	my $file=$confdir."messages_forwarded_$kid_index";
	tie(my %messages_forwarded, 'NDBM_File', $file, O_RDWR|O_CREAT, 0600);
	if($newmessages eq 'wilmatrix') {
		@new=&new_messages_wilmatrix($kid_index,\%messages_forwarded);
	}else{
		@new=&new_messages_wilma($kid_index);
	}
	for my $message_id (@new) {
		my $html=&get_message($kid_index,$message_id);
		if ($html) {
			my ($text,$body)=&parse_message($html);
			if (defined($text)) {
				&send_html($kid_index,$text,$body,'messages') or return '';
				if(!$stdout) {
					$messages_forwarded{$message_id}=time;
				}
			}
		}else{
			return '';
		}
	}
	return 1;
}

sub forward_attendance {
	my $kid_index=shift;
	my $html=&get_attendance($kid_index);
	$html or return '';
	my $count=&parse_attendance(\$html);
	warn "$count attendance markings\n" if $debug;
	my $file=$confdir.'attendance.txt';
	my $adate;
	my $acolumn;
	if(open(A,'<',$file)) {
		$adate=<A>;
		chomp $adate;
		$acolumn=<A>;
		chomp $acolumn;
	}else{
		$adate='1.1.1977';
		$acolumn=0;
	}
	my @new=&sort_attendance($adate, $acolumn);
	my $sent=0;
	for my $a (@new) {
		my $expl=$attendance_expl{$a->{'class'}};
		if(!defined($expl)) {
			$expl=$a->{'class'};
		}
		&send_text($kid_index,'Tuntimerkintä: '.$expl.' '.$a->{'wd'}.' '.$a->{'date'}.' '.$a->{'title'},'attendance');
		++$sent;
	}
	if(!$stdout and $sent) {
		open(A,'>',$file) or warn "Can't open $file for writing: $!";
		print A $new[-1]->{'date'},"\n";
		print A $new[-1]->{'column'};
		close A;
	}
	return 1;
}

sub sort_attendance {
	my ($d,$c)=@_;
	my $limit={};
	$limit->{'date'}=$d;
	$limit->{'column'}=$c;
	my $end=1+$#attendance;
	for(my $i=0; $i < $end ; ) {
		my $href=$attendance[$i];
		my $cmp=&cmp_att($href, $limit);
		if($cmp <= 0) {
			splice @attendance,$i,1;
			$end=1+$#attendance;
			warn "not sending ",$href->{'date'},"\n" if $debug;
		}else{
			++$i;
		}
	}
	return sort cmp_att @attendance;
}

sub cmp_att($$) {
	my ($a,$b)=@_;
	my $ad=$a->{'date'};
	my $bd=$b->{'date'};
	my $cmp=&compare_dates($ad,$bd);
	$cmp != 0 and return $cmp;
	my $ac=$a->{'column'};
	my $bc=$b->{'column'};
	return $ac <=> $bc;
}

sub compare_dates {
	my ($a,$b)=@_;
	$a=~/^(\d+)\.(\d+)\.(\d+)/;
	my ($ay,$am,$ad)=($3,$2,$1);
	$b=~/^(\d+)\.(\d+)\.(\d+)/;
	my ($by,$bm,$bd)=($3,$2,$1);
	my $cmp=$ay <=> $by;
	$cmp != 0 and return $cmp;
	$cmp=$am <=> $bm;
	$cmp != 0 and return $cmp;
	return $ad <=> $bd;
}

sub forward_news {
	my $kid_index=shift;
	my $html=&get_news($kid_index);
	$html or return '';
	my $news_found=&parse_news(\$html);
	if($news_found==0) {
		warn "no news found\n";
		return;
	}
	my $file=$confdir."news_forwarded_$kid_index";
	tie(my %news_forwarded, 'NDBM_File', $file, O_RDWR|O_CREAT, 0600);
	for(my $i=$#news_title ; $i >= 0 ; $i--) {
		my $key=$news_title[$i];
		my $value=$news_link{$key};
		if($value=~/^\/(.+)$/) {
			$value=$w.$1;
		}
		if($value =~ /news\/(\d+)$/) {
			my $news_id=$1;
			if(defined($news_forwarded{$news_id})) {
				warn "News item $news_id already forwarded.\n" if $debug;
			}else{
				if(&forward_news_item($kid_index, $key, $value)) {
					if($stdout) {
						warn "--stdout given on command line. NOT marking news item $news_id as read\n";
					}else{
						$news_forwarded{$news_id}=1;
					}
				}
			}
		}else{
			warn "Unknown link: $value\n";
		}
	}
	return 1;
}
sub get_news_item {
	my $url=shift;
	if($url=~/^(.*news\/)(\d+)$/) {
		$url=$1.'printable/'.$2;
	}else{
		warn "Unknown news url: \"$url\"\n";
		return;
	}
	my $r=$ua->get($url);
	if($r->is_success) {
		return $r->decoded_content;
	}
	warn "Error fetching news item \"$url\": ",$r->code,' ',$r->message,' ',$r->decoded_content,"\n";
	return;
}
sub forward_news_item {
	my ($kid_index,$title, $url)=@_;
	my $html=get_news_item($url);
	my ($text,$body)=&parse_message($html);
	if (defined($text)) {
		&send_html($kid_index,$text,$body,'news');
	}
}

sub ensurelogin {
	$login_done and return 1;
	&read_config;
	&get_sessionid;
	return &post_login;
}

sub main {
	&ensurelogin or die;
	my $last_kid=$#kid_id;
	$SIG{'INT'}=sub { $loop=0; };
	$SIG{'TERM'}=sub { $loop=0; };
	do {
		for (my $kid_index=0 ; $kid_index <= $last_kid; $kid_index++) {
			&forward_messages($kid_index) or $loop=0;
			&forward_news($kid_index) or $loop=0;
			&forward_attendance($kid_index) or $loop=0;
		}
		if($loop) {
			sleep $loop;
		}
	}while($loop);
}

my @main=();
my @args=();
while(defined(my $a=shift)) {
	if($a eq '--nothing') {
		exit 0;
	}elsif($a eq '--debug') {
		$debug=1;
		&debug_headers;
	}elsif($a eq '--test') {
		push @main,\&test;
	}elsif($a eq '--stdout') {
		$stdout=1;
	}elsif($a eq '--catchup-messages') {
		my $kid_index=shift;
		defined($kid_index) or die;
		push @main,\&ensurelogin,\&get_message_list,\&mark_all_messages_as_read;
		$args[$#main -1]=[$kid_index];
		$args[$#main]=[$kid_index];
	}elsif($a eq '--mark-message-unread') {
		my $kid_index=shift;
		my $message_id=shift;
		defined($message_id) or die;
		push @main,\&mark_message_unread;
		$args[$#main]=[$kid_index,$message_id];
	}elsif($a eq '--list-read-messages') {
		push @main,\&list_read_messages;
	}elsif($a eq '--read-config') {
		push @main,\&read_config;
	}elsif($a eq '--') {
		last;
	}else{
		die "$0: Unknown \"$a\" on command line.\n";
	}
}
if(!@main) {
	@main=(\&main);
}
my $i=0;
for my $f (@main) {
	&{$f}(@{$args[$i]}) or last;
	$i++;
}
if($login_done) {
	&post_logout;
}



sub test {
	#&read_config;
	#&test_parse_news;
	&test_parse_attendance;
	#&test_logout('/tmp/Oma etusivu - Wilma.html');
}

sub test_get_list {
	&read_config;
	&test_login;
	&get_message_list(0);
	&post_logout;
}

sub test_parse_sessionid {
    open(F,'/tmp/wilma_login.html');
    my $foo=join('',<F>);
    utf8::decode($foo);
    warn &parse_sessionid($foo);
}

sub test_list_json {
	open(J, '<', '/tmp/wilma_list.json');
	my $j=decode_json(join('',<J>));
}

sub test_login {
	&get_sessionid;
	warn "sessionid=$sessionid\n";
	$cookie_jar->save;
	defined($sessionid) or die $!;
	&post_login;
}

sub test_parse_logout {
	open(IN, '<', shift);
	my $html=join('',<IN>);
	&parse_logout($html);
	warn $logout_value;
}

sub test_parse_message {
	open(IN, '</tmp/ke.html');
	my $html=join('',<IN>);
	my ($text,$body)=&parse_message($html);
	warn $text;
	warn $body;
}

sub test_parse_news {
	open(IN, '</tmp/Tiedotteet.html') or die $!;
	my $html=join('',<IN>);
	utf8::decode($html);
	warn &parse_news(\$html);
	for my $t (@news_title) {
		warn "$t: $news_link{$t}\n";
	}
}

sub test_parse_attendance {
	open(IN, '</tmp/Tuntimerkinnät - Wilma.html') or die $!;
	my $html=join('',<IN>);
	utf8::decode($html);
	my $count=&parse_attendance(\$html);
	warn "$count attendance markings\n";
	my $array=\@attendance;
	my @new;
	if(defined($ARGV[1])) {
		@new=&sort_attendance(@ARGV);
		$array=\@new;
	}
	for my $href (@$array) {
		warn "\n";
		for my $key (sort keys %$href) {
			my $value=$href->{$key};
			if($key eq 'class') {
				warn "$key: ",$attendance_expl{$value},"\n";
			}else{
				utf8::encode($value);
				warn "$key: $value\n";
			}
		}
	}
}
