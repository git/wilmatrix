Wilmatrix - viestinvälitys Wilmasta Matrixiin
---------------------------------------------

Ominaisuudet:

- uudet viestit
- tiedotteet
- tuntimerkinnät


Asennus lyhyesti:

- sudo apt install libfile-homedir-perl libhtml-formattext-withlinks-perl libhtml-parser-perl libhttp-cookies-perl libjson-perl libwww-perl
- sudo apt install libterm-readkey-perl (Ei pakollinen, tarpeen vain jos salasana ei ole asetustiedostossa.)
- sudo install wilmatrix /usr/local/bin
- mkdir -p ~/.config/wilmatrix
- editor ~/.config/wilmatrix/wilmatrix.conf
- chmod og-rwx ~/.config/wilmatrix/wilmatrix.conf
- crontab -e

wilmatrix.conf
--------------

domain=foo.inschool.fi
user=käyttäjätunnus_wilmassa
pass=salasana_wilmassa
kid0=1234
kid1=5678
loop=1000
matrix_url0=https://matrixpalvelin/_matrix/client/r0/rooms/%21HUONEEN_TUNNISTE:matrix.osoi.te/send/m.room.message?access_token=ACCESS_TOKEN
matrix_url1=https://matrixpalvelin/_matrix/client/r0/rooms/%21TOISEN_HUONEEN_TUNNISTE:matrix.osoi.te/send/m.room.message?access_token=ACCESS_TOKEN
matrix_url_messages0=...
matrix_url_news0=...
useragent=wilmalle_ilmoitettava_useragent
newmessages=wilmatrix

Asetusten selitys:

domain: Wilman osoite ilman https://:aa ja lopussa kauttaviivaa.

user: Käyttäjätunnus wilmassa, monesti sähköpostiosoite.

pass: Salasana, jolla kirjaudut wilmaan.

loop: Hakujen toiston aikaväli sekunteina. Jos nolla, wilmatrix kirjautuu ulos ja lopettaa yhden haun jälkeen.
Aikavälin on oltava riittävän lyhyt, jottei Wilma kirjaa käyttäjää automaattiseti ulos.

kid0, kid1, ..., kidN: Lasten tunnisteet wilmassa. Ne löytää, kun menee
wilmassa viesteihin ja katsoo, mikä luku on osoiterivillä, esim.
https://foo.inschool.fi/!1234567/messages

matrix_url0, matrix_url1, ...: Osoitteet, johon eri lasten viestit lähetetään.
Huoneiden tunnisteet löytää esim. Element webin osoiterivin lopusta. Jos
osoiterivi loppuu esim. 
.../#/room/!abcde:matrix.xyz
niin ylläoleva HUONEEN_TUNNISTE:matrix.osoi.te korvataan: abcde:matrix.xyz

Lisäämällä asetustiedostoon rivejä tyyliin
matrix_url_messages0=
matrix_url_news0=
matrix_url_attendance0=
viestit, tiedotteet ja tuntimerkinnät voi ohjata eri huoneisiin.

useragent: Wilma ei tykkää siitä, että selaimen user agentiksi ilmoitetaan libwww-perl (kiitos luultavasti wilmatrixin), joten 
useragentiksi kannattaa asentaa jotakin muuta.

newmessages:
  newmessages=wilmatrix : wilmatrix pitää kirjaa siitä, mitkä viestit on lähetetty matrixiin
  newmessage=wilma : wilmatrix lähettää matrixiin ne viestit, jotka wilma ilmoittaa uusiksi viesteiksi

Uuden ACCESS_TOKENin saa kirjautumalla sisään matrixiin curlilla, esim.
curl -XPOST -d '{"type":"m.login.password", "user":"KÄYTTÄJÄTUNNUS","password":"SALASANA"}' https://matrixpalvelin/_matrix/client/r0/login


Komentorivi
-----------

Normaalissa käytössä wilmatrix lukee kaiken tarvittavan asetushakemistosta,
eikä tarvitse lisämääreitä komentorivillä. Seuraavat valinnat ovat kuitenkin
käytettävissä:


--stdout

Ei lähetä mitään matrixiin. Ei päivitä tietoa luetuista tiedotteista.
Uudet viestit saattavat kuitenkin Wilmassa tulla merkityksi luetuksi.


--debug

Tulostaa suuren määrän turhaa tietoa ajon aikana.


--nothing

Ei tee yhtään mitään. (Paitsi tarkistaa, että koodissa ei ole syntaksivirheitä)


--test

Ajaa normaalin toiminnan sijaan sijaan "sub test":n. Useimmiten tällä testataan
uusinta keskeneräistä ominaisuutta Wilmasta /tmp:iin tallennetun tiedoston
kanssa.


--catchup-messages kid_index

Merkitsee kaikki viestit luetuiksi, eikä välitä niitä Matrixiin. kid_index on esim. 0 tai 1


--mark-message-unread kid_index message_id

Merkitsee yhden viestin lukemattomaksi yhden lapsen osalta.


--list-read-messages

Kertoo, minkänumeroiset viestit on merkitty luetuiksi.
Toimii vain, kun wilmatrix.conf:issa on 
newmessages=wilmatrix


crontab
-------

Käynnistettäessä Wilmatrix kirjautuu Wilmaan, tarkistaa viestit,
välittää ne Matrixiin ja lopettaa. Sitä pitää siis ajaa toistuvasti,
esim. muutaman kerran päivässä. Esimerkki crontab-rivistä:

58 7,9,11,13,15,17,20 * * 1-5 /home/mijutu/bin/wilmatrix

Cronin sijaan voi tietekin käyttää myös jotakin muuta ajastinta,
kuten systemd.timeriä, tai virittää wilmatrixin ajon 
muistutussähköpostin saapuessa .procmailrc:llä


procmail
--------
Esimerkki wilmatrixin käynnistämisetä procmaililla

Lähetetään kopio wilman viesteistä toiseen osoitteeseen wilmatrixia varten

:0 c
* ^From:.*foo.wilma@edu.foobar.fi
!wilmatrix@mullekotio.dy.fi

Toisella koneella sähköposti syötetään skriptille, joka
käynistää wilmatrixin

:0
* ^From.*foo.wilma@edu.foobar.fi
|/home/minä/bin/_wilmatrix_procmail

_wilmatrix_procmail:

#!/bin/sh
/home/minä/bin/wilmatrix </dev/null >/dev/null 2>&1


Taustatietoa
------------

Wilmatrix on tehty reverse-engineering-menegelmällä, katsomalla 
Nettisivun lähdekoodista ja firefoxin inspectorista, miten 
Wilmaan kirjaudutaan sisään ja missä muodossa viestiluettelo
ja viestit ovat. 

Matrixiin välittyy wilmaviestin "Tulostettava versio" html-viestinä.
Ja lisäksi html:stä muunnettu tekstiversio.

